# mkwinstdrv

## Requirements

- dosfstools
- ntfs-3g
- parted
- udisks2

## Usage

```
./mkwinstdrv.sh [options]
Options:
  -d, --device <device> Set the storage device block.
  -f, --fast            Skip zero fill and bad block check.
  -h, --help            Show this help message.
  -i, --image <image>   Set the windows image.
```
