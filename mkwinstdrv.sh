#!/usr/bin/env bash

SCRIPT_DIR="$PWD"

fatArgs="-c"
ntfsArgs=""

imageFile=""
imageLoopDevice=""
imageMountPoint=""

storageBlockDevice=""
efiMountPoint=""
dataMountPoint=""

main()
{
  check_dependencies

  read_arguments "$@"

  printf "Formatting $storageBlockDevice.\n"
  format_storage "$storageBlockDevice"

  printf "Mounting $storageBlockDevice.\n"
  mount_storage "$storageBlockDevice"

  printf "Mounting $imageFile.\n"
  mount_image "$imageFile"

  printf "Copying files. This might take a while.\n"
  copy_image_files_to_storage
}

display_help()
{
  printf "\nmkwinstdrv\n"
  printf "Copyright (c) Lester Espiritu\n\n"
  printf "Usage:\n"
  printf "  ./mkwinstdrv.sh [options]\n\n"
  printf "Options:\n"
  printf "  -d, --device <device>\t\tSet the storage device block.\n"
  printf "  -f, --fast\t\t\tSkip zero fill and bad block check.\n"
  printf "  -h, --help\t\t\tShow this help message.\n"
  printf "  -i, --image <image>\t\tSet the windows image.\n\n"
}

read_arguments()
{
  [ "$#" -ne "0" ] || { printf "No arguments specified.\n" ; exit 1 ; }

  while [ "$#" -gt 0 ]
  do
    arg="$1"
    case "$arg" in
      "-d" | "--device")
        storageBlockDevice="$2"
        shift
        [ "$#" -gt 0 ] && shift
      ;;
      "-i" | "--image")
        imageFile="$2"
        shift
        [ "$#" -gt 0 ] && shift
      ;;
      "-f" | "--fast")
        fatArgs=""
        ntfsArgs="-f"
        shift
      ;;
      "-h" | "--help")
        display_help; exit
      ;;
      *)
        echo "$1 is not a supported argument."; exit 1
      ;;
    esac
  done

  [ -b "$storageBlockDevice" ] || { printf "Could not find block device $2.\n" ; exit 1 ; }
  [ -f "$imageFile" ] || { printf "Could not find image $2.\n" ; exit 1 ; }
}

check_dependencies()
{
  dependencies_installed=1

  command -v parted &> /dev/null || { printf "Could not find parted.\n" ; dependencies_installed=0 ; }
  command -v mkfs.fat &> /dev/null || { printf "Could not find dosfstools.\n" ; dependencies_installed=0 ; }
  command -v mkfs.ntfs &> /dev/null || { printf "Could not find ntfs-3g.\n" ; dependencies_installed=0 ; }
  command -v udisksctl &> /dev/null || { printf "Could not find udisks2.\n" ; dependencies_installed=0 ; }

  [ "$dependencies_installed" -eq "1" ] || { printf "Please install the necessary dependency packages.\n" ; exit 1 ; }
}

assert()
{
  [ "$?" -eq "0" ] || { printf "Error occured.\n" ; exit 1 ; }
}

format_storage()
{
  sudo sh -c "parted -s '$1' mklabel gpt && \
              parted -s '$1' mkpart 'EFI' fat32 0% 1GiB && \
              parted -s '$1' mkpart 'DATA' ntfs 1GiB 100% && \
              mkfs.fat $fatArgs -n 'EFI' -F 32 '${1}1' && \
              mkfs.ntfs $ntfsArgs -L 'DATA' '${1}2'"

  assert
}

mount_storage()
{
  IFS=$' ' efiMountInfo=($(udisksctl mount -b "${1}1"))
  assert

  efiMountPoint="${efiMountInfo[3]}"

  IFS=$' ' dataMountInfo=($(udisksctl mount -b "${1}2"))
  assert

  dataMountPoint="${dataMountInfo[3]}"
}

unmount_storage()
{
  udisksctl unmount -b "${1}1" &> /dev/null
  udisksctl unmount -b "${1}2" &> /dev/null
}

mount_image()
{
  IFS=$' ' loopInfo=($(udisksctl loop-setup -f "$1"))
  assert

  imageLoopDevice="${loopInfo[4]%?}"

  IFS=$' ' mountInfo=($(udisksctl mount --options ro -b "$imageLoopDevice"))
  assert

  imageMountPoint="${mountInfo[3]}"
}

unmount_image()
{
  udisksctl unmount -b "$1" &> /dev/null
  udisksctl loop-delete -b "$1" &> /dev/null
}

copy_image_files_to_storage()
{
  cd "$imageMountPoint"

  find * -path "sources" -prune -o -exec cp --parents -r "{}" "$efiMountPoint" \;
  cp --parents "sources/boot.wim" "$efiMountPoint"

  cd "$imageMountPoint/sources"

  mkdir "$dataMountPoint/sources"
  find * -name "boot.wim" -prune -o -exec cp --parents -r "{}" "$dataMountPoint/sources" \;

  cd "$SCRIPT_DIR"
}

clean_up()
{
  { [ "$imageMountPoint" = "" ] && [ "$efiMountPoint" = "" ] && [ "$dataMountPoint" = "" ] ; } || \
    { printf "Cleaning up.\n"; unmount_storage "$storageBlockDevice"; unmount_image "$imageLoopDevice" ; }

  [ ! $(mount | grep "$storageBlockDevice" &> /dev/null) ] || printf "Could not unmount storage. Check logs.\n"
  [ ! $(mount | grep "$imageLoopDevice" &> /dev/null) ] || printf "Could not unmount image. Check logs.\n"
}

trap clean_up EXIT

main "$@"; exit
